import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgReduxModule, NgRedux } from '@angular-redux/store';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MdButtonModule,
  MdCheckboxModule,
  MdToolbarModule,
  MdInputModule,
  MdCardModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { TodoComponent } from './pages/todo/todo.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { store, IAppState, TodoActions } from './store';
import { TodoItemComponent } from './components/todo-item/todo-item.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodoListComponent,
    TodoItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgReduxModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdCheckboxModule,
    MdToolbarModule,
    MdInputModule,
    MdCardModule,
    RouterModule.forRoot([
      {
        path: 'todo',
        component: TodoComponent
      },
      {
        path: '',
        redirectTo: 'todo',
        pathMatch: 'full'
      }
    ])
  ],
  providers: [
    TodoActions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.provideStore(store);
  }
}
