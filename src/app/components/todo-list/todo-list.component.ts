import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from 'app/models/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {
  @Input() todos: Todo[];
  @Output() newTodoAdded = new EventEmitter<Todo>();

  newTodoText: string;

  addNewTodo() {
    const todo = {
      title: this.newTodoText,
      isDone: false
    } as Todo;

    this.newTodoText = '';

    this.newTodoAdded.emit(todo);
  }
}
