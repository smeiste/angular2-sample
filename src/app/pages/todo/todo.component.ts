import { Component } from '@angular/core';
import { IAppState, TodoActions } from '../../store';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import { Todo } from 'app/models/todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent {
  @select('todos') todos$: Observable<Todo[]>;

  constructor(
    private ngRedux: NgRedux<IAppState>,
    private todoActions: TodoActions
  ) { }

  onNewTodo(todo: Todo) {
    this.todoActions.addTodo(todo);
  }

}
