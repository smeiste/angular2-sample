import {Todo} from '../models/todo';

export class IAppState {
    todos: Todo[];
}
