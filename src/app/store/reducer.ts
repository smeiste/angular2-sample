import { Todo } from '../models/todo';
import { IAppState } from './IAppState';
import { TODOS_ADD } from './todo.actions'

const initialState: IAppState = {
    todos : [
        {title: 'Walk the dog', isDone: false},
        {title: 'Get new job', isDone: false}
    ]
};

function addNewTodo(state, action): IAppState {
    return Object.assign({}, state, {
        todos: [...state.todos, action.todo]
    });
}

export function reducer(state = initialState, action) {
    switch(action.type) {
        case TODOS_ADD:
            return addNewTodo(state, action);
        default:
            return state;
    }
}
