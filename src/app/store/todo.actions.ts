import { NgRedux } from '@angular-redux/store';
import { IAppState } from './IAppState';
import { Injectable} from '@angular/core';
import { Todo } from '../models/todo';

export const TODOS_GET = 'todos_get';
export const TODOS_ADD = 'todos_add';

// create injectable class. If we had http services, we would execute
// them from here and dispatch the results to our store.
@Injectable()
export class TodoActions {

    constructor(private ngRedux: NgRedux<IAppState>) {}

    // dispatch our add action to the store
    addTodo(todo: Todo) {
        this.ngRedux.dispatch({
            type: TODOS_ADD,
            todo
        });
    }
}
